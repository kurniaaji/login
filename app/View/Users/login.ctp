<?php
echo $this->Form->create('User', array(
  'url'   => array(
       'controller' => 'users','action' => 'login'
   ), 
  'id'    => 'web-form', 
  'class' =>'panel-body wrapper-lg'
));
echo $this->Form->inputs(array(
    'legend' => __('Login'),
    'username',
    'password'
));
echo $this->Form->end('Login');
?>